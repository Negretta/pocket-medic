/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Roles;
import com.pocketmedic.jpa.entities.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Luis
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "PM-BackendPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }

    public Usuarios findByEmail(String email) {
        try {
            return (Usuarios) em.createNamedQuery("Usuarios.findByEmail")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }

    public List<Usuarios> findUsuarioByIdRol(String idRol) {
        try {
            return getEntityManager().createNamedQuery("Usuarios.findByIdRol")
                    .setParameter("idRol", idRol)
                    .getResultList();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }

    public List<Usuarios> findUsuarioBy(String idRol) {
        try {
            return getEntityManager().createNativeQuery("select c.descripcion, c.usuario, c.fecha_con from CONSULTAS c "
                    + "inner JOIN USUARIOS u "
                    + "inner JOIN ROLES r "
                    + "WHERE u.id_usuario = c.medico and u.id_rol = r.id_rol and r.id_rol=MEDIC")
                    .getResultList();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }

    public List<Roles> findRolesByIdUsuario(Integer idUsuario) {
        try {
            return getEntityManager().createNativeQuery("select r.nombre_rol "
                    + "from USUARIOS u "
                    + "inner JOIN ROLES r "
                    + "WHERE u.id_rol = r.id_rol "
                    + "AND u.id_usuario = :idUsuario")
                    .setParameter("idUsuario", idUsuario)
                    .getResultList();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }

}
