/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.ProfesionesUsuarios;
import com.pocketmedic.jpa.entities.ProfesionesUsuariosPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author Luis
 */
@Stateless
@Path("profesionesusuarios")
public class ProfesionesUsuariosFacadeREST extends AbstractFacade<ProfesionesUsuarios> {
    @PersistenceContext(unitName = "PM-BackendPU")
    private EntityManager em;

    private ProfesionesUsuariosPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idProfesionUsuario=idProfesionUsuarioValue;idProfesiones=idProfesionesValue;idUsuario=idUsuarioValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        com.pocketmedic.jpa.entities.ProfesionesUsuariosPK key = new com.pocketmedic.jpa.entities.ProfesionesUsuariosPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idProfesionUsuario = map.get("idProfesionUsuario");
        if (idProfesionUsuario != null && !idProfesionUsuario.isEmpty()) {
            key.setIdProfesionUsuario(new java.lang.Integer(idProfesionUsuario.get(0)));
        }
        java.util.List<String> idProfesiones = map.get("idProfesiones");
        if (idProfesiones != null && !idProfesiones.isEmpty()) {
            key.setIdProfesiones(new java.lang.Integer(idProfesiones.get(0)));
        }
        java.util.List<String> idUsuario = map.get("idUsuario");
        if (idUsuario != null && !idUsuario.isEmpty()) {
            key.setIdUsuario(new java.lang.Integer(idUsuario.get(0)));
        }
        return key;
    }

    public ProfesionesUsuariosFacadeREST() {
        super(ProfesionesUsuarios.class);
    }

    @POST
    @Override
    @Consumes({"application/json"})
    public void create(ProfesionesUsuarios entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") PathSegment id, ProfesionesUsuarios entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        com.pocketmedic.jpa.entities.ProfesionesUsuariosPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public ProfesionesUsuarios find(@PathParam("id") PathSegment id) {
        com.pocketmedic.jpa.entities.ProfesionesUsuariosPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<ProfesionesUsuarios> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<ProfesionesUsuarios> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
