(function () {
    'use strict';

    angular.module('app.consultasCreate', [
        'app.consultasCreate.services',
        'app.consultasCreate.route',
        'app.consultasCreate.controller',
        'app.consultasCreate.directivas'
    ]);

})();
