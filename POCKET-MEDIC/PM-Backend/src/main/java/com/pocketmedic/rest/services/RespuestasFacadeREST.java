/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.Respuestas;
import com.pocketmedic.jpa.sessions.RespuestasSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Luis
 */
@Stateless
@Path("respuestas")
public class RespuestasFacadeREST {
    @EJB
    private RespuestasSession ejbRespuesta;
    @POST
    @Consumes({"application/json"})
    public void create(Respuestas respuestas) {
        ejbRespuesta.create(respuestas);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, Respuestas respuestas) {
        ejbRespuesta.update(respuestas);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbRespuesta.remove(ejbRespuesta.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Respuestas find(@PathParam("id") Integer id) {
        return ejbRespuesta.find(id);
    }

    @GET
    @Produces({"application/json"})
    public List<Respuestas> findAll() {
        return ejbRespuesta.findAll();
    }

}
