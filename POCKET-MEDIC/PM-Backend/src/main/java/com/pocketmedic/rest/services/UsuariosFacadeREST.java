/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pocketmedic.jpa.entities.Usuarios;
import com.pocketmedic.jpa.sessions.UsuariosSession;
import com.pocketmedic.rest.auth.DigestUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 *
 * @author Luis
 */
@Stateless
@Path("usuarios")
public class UsuariosFacadeREST {

    @EJB
    private UsuariosSession ejbUsuariosFacade;

    
   @POST
   @Consumes({"application/json"})
   public Response create(Usuarios usuarios){
       GsonBuilder builder = new GsonBuilder();
       Gson gson = builder.create();
       if(ejbUsuariosFacade.findByEmail(usuarios.getEmail())==null){
           
           usuarios.setPassword(DigestUtil.cifrarPassword(usuarios.getPassword()));
            System.out.println("PASSWORD CIFRADA");
            System.out.println(usuarios.getPassword());
           
        ejbUsuariosFacade.create(usuarios);
        return Response.ok()
                .entity(gson.toJson("ACEPTADO"))
                .build();  

        }else{
          return Response
                  .status(Response.Status.CONFLICT)
                  .entity(gson.toJson("el email ya se encuentra registrado"))
                  .build();  
        
    
    }
   }
    
    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, Usuarios usuarios) {
        System.out.println("PASSWORD");
        System.out.println(usuarios.getPassword());
        ejbUsuariosFacade.update(usuarios);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbUsuariosFacade.remove(ejbUsuariosFacade.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Usuarios find(@PathParam("id") Integer id) {
        return ejbUsuariosFacade.find(id);
    }

    @GET
    @Path("rol/{id}")
    @Produces({"application/json"})
    public List<Usuarios> findUsuarioByIdRol(@PathParam("id") String id) {
        System.out.println(id);
        return ejbUsuariosFacade.findUsuarioByIdRol(id);
    }

    @GET
    @Produces({"application/json"})
    public List<Usuarios> findAll() {
        return ejbUsuariosFacade.findAll();
    }

   

}
