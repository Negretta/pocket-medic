/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.Roles;
import com.pocketmedic.jpa.sessions.RolesSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Luis
 */
@Stateless
@Path("roles")
public class RolesFacadeREST {
    @EJB
    private RolesSession ejbRoles;
    
    @POST
    @Consumes({"application/json"})
    public void create(Roles roles) {
        ejbRoles.create(roles);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") String id, Roles roles) {
        ejbRoles.update(roles);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        ejbRoles.remove(ejbRoles.find(0));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Roles find(@PathParam("id") String id) {
        return ejbRoles.find(0);
    }

    @GET
    @Produces({"application/json"})
    public List<Roles> findAll() {
        return ejbRoles.findAll();
    }

}
