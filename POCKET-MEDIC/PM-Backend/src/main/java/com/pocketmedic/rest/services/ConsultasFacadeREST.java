/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.Consultas;
import com.pocketmedic.jpa.sessions.ConsultasSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author :V
 */
@Stateless
@Path("consultas")
public class ConsultasFacadeREST {
    
    @EJB
    private ConsultasSession ejbConsultas;
    
    @POST
    @Consumes({"application/json"})
    public void create(Consultas consultas) {
        ejbConsultas.create(consultas);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, Consultas consultas) {
        ejbConsultas.update(consultas);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbConsultas.remove(ejbConsultas.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Consultas find(@PathParam("id") Integer id) {
        return ejbConsultas.find(id);
    }

    @GET
    @Produces({"application/json"})
    public List<Consultas> findAll() {
        return ejbConsultas.findAll();
    }
}
