/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.TitulosYCertificados;
import com.pocketmedic.jpa.sessions.TitulosYCertificadosSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Luis
 */
@Path("titulosycertificados")
public class TitulosYCertificadosFacadeREST {
    @EJB
    private TitulosYCertificadosSession ejbTitulosYCerFacade;


    @POST
    @Consumes({"application/json"})
    public void create(TitulosYCertificados titulosycertificados) {
        ejbTitulosYCerFacade.create(titulosycertificados);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, TitulosYCertificados titulosycertificados) {
        ejbTitulosYCerFacade.update(titulosycertificados);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbTitulosYCerFacade.remove(ejbTitulosYCerFacade.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public TitulosYCertificados find(@PathParam("id") Integer id) {
        return ejbTitulosYCerFacade.find(id);
    }

    @GET
    @Produces({"application/json"})
    public List<TitulosYCertificados> findAll() {
        return ejbTitulosYCerFacade.findAll();
    }

    @GET
    @Path("nombre/{query}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<TitulosYCertificados> findByTitulos(@PathParam("query") String titulos) {
        return ejbTitulosYCerFacade.findByTitulos(titulos);
    }
}
