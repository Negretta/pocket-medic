/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.Ciudades;
import com.pocketmedic.jpa.entities.CiudadesPK;
import com.pocketmedic.jpa.sessions.CiudadesSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author Luis
 */
@Stateless
@Path("ciudades")
public class CiudadesREST {

    @EJB
    private CiudadesSession ejbCiudadesFacade;

    private CiudadesPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idCiudad=idCiudadValue;idDepartamento=idDepartamentoValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        com.pocketmedic.jpa.entities.CiudadesPK key = new com.pocketmedic.jpa.entities.CiudadesPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idCiudad = map.get("idCiudad");
        if (idCiudad != null && !idCiudad.isEmpty()) {
            key.setIdCiudad(new java.lang.Integer(idCiudad.get(0)));
        }
        java.util.List<String> idDepartamento = map.get("idDepartamento");
        if (idDepartamento != null && !idDepartamento.isEmpty()) {
            key.setIdDepartamento(new java.lang.Integer(idDepartamento.get(0)));
        }
        return key;
    }


    @POST
    @Consumes({"application/json"})
    public void create(Ciudades ciudades) {
        ejbCiudadesFacade.create(ciudades);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") PathSegment id, Ciudades ciudades) {
        ejbCiudadesFacade.update(ciudades);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        com.pocketmedic.jpa.entities.CiudadesPK key = getPrimaryKey(id);
        ejbCiudadesFacade.remove(ejbCiudadesFacade.find(0));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Ciudades find(@PathParam("id") PathSegment id) {
        com.pocketmedic.jpa.entities.CiudadesPK key = getPrimaryKey(id);
        return ejbCiudadesFacade.find(0);
    }

    @GET
    @Produces({"application/json"})
    public List<Ciudades> findAll() {
        return ejbCiudadesFacade.findAll();
    }


    @GET
    @Path("nombre/{nombre}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Ciudades> findByNombre(@PathParam("nombre") String nombre) {
        return ejbCiudadesFacade.findByNombre(nombre);
    }

}
