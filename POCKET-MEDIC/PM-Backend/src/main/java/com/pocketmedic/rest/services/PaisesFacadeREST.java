/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.Paises;
import com.pocketmedic.jpa.sessions.PaisesSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author Luis
 */
@Stateless
@Path("paises")
public class PaisesFacadeREST {
    @EJB
    private PaisesSession ejbPaises;
    
    @POST
    @Consumes({"application/json"})
    public void create(Paises paises) {
        ejbPaises.create(paises);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") String id, Paises paises) {
        ejbPaises.update(paises);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        ejbPaises.remove(ejbPaises.find(0));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Paises find(@PathParam("id") String id) {
        return ejbPaises.find(0);
    }

    @GET
    @Produces({"application/json"})
    public List<Paises> findAll() {
        return ejbPaises.findAll();
    }
    
}
