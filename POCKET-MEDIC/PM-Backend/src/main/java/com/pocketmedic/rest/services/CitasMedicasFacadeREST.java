/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.rest.services;

import com.pocketmedic.jpa.entities.CitasMedicas;
import com.pocketmedic.jpa.sessions.CitasMedicasSession;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author :V
 */
@Stateless
@Path("citasmedicas")
public class CitasMedicasFacadeREST {
    
    @EJB
    private CitasMedicasSession ejbCitasMedicas;
    
    @POST
    @Consumes({"application/json"})
    public void create(CitasMedicas citasmedicas) {
        ejbCitasMedicas.create(citasmedicas);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/json"})
    public void edit(@PathParam("id") Integer id, CitasMedicas citasmedicas) {
        ejbCitasMedicas.update(citasmedicas);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        ejbCitasMedicas.remove(ejbCitasMedicas.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public CitasMedicas find(@PathParam("id") Integer id) {
        return ejbCitasMedicas.find(id);
    }

    @GET
    @Produces({"application/json"})
    public List<CitasMedicas> findAll() {
        return ejbCitasMedicas.findAll();
    }

}
