/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Departamentos;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class DepartamentosSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Departamentos departamentos){
        entityManager.persist(departamentos);
    }
    
    public void update (Departamentos departamentos){
        entityManager.merge(departamentos);
    }
    
    public void remove (Departamentos citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Departamentos find(int id){
        return getEntityManager().find(Departamentos.class, id);
    }
    
    public List<Departamentos> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Departamentos.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
