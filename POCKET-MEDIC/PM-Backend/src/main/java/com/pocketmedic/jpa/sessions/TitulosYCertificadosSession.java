/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.TitulosYCertificados;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class TitulosYCertificadosSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    
    public void create (TitulosYCertificados titulosycertificados){
        entityManager.persist(titulosycertificados);
    }
    
    public void update (TitulosYCertificados titulosycertificados){
        entityManager.merge(titulosycertificados);
    }
    
    public void remove (TitulosYCertificados citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public TitulosYCertificados find(int id){
        return getEntityManager().find(TitulosYCertificados.class, id);
    }
    
    public List<TitulosYCertificados> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(TitulosYCertificados.class));
        return entityManager.createQuery(cq).getResultList();
    }
    
    public List<TitulosYCertificados> findByTitulos(String titulos) {
        return getEntityManager().createNamedQuery("TitulosYCertificados.findByTitulos")
                .setParameter("titulos", titulos + "%")
                .getResultList();
    }
}
