/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Respuestas;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class RespuestasSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Respuestas respuestas){
        entityManager.persist(respuestas);
    }
    
    public void update (Respuestas respuestas){
        entityManager.merge(respuestas);
    }
    
    public void remove (Respuestas citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Respuestas find(int id){
        return getEntityManager().find(Respuestas.class, id);
    }
    
    public List<Respuestas> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Respuestas.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
