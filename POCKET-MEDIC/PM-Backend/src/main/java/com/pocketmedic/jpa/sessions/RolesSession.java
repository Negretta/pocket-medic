/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Roles;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class RolesSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Roles roles){
        entityManager.persist(roles);
    }
    
    public void update (Roles roles){
        entityManager.merge(roles);
    }
    
    public void remove (Roles citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Roles find(int id){
        return getEntityManager().find(Roles.class, id);
    }
    
    public List<Roles> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Roles.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
