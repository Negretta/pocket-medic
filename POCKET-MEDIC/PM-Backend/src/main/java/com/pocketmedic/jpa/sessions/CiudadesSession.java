/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Ciudades;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class CiudadesSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Ciudades ciudades){
        entityManager.persist(ciudades);
    }
    
    public void update (Ciudades ciudades){
        entityManager.merge(ciudades);
    }
    
    public void remove (Ciudades citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Ciudades find(int id){
        return getEntityManager().find(Ciudades.class, id);
    }
    
    public List<Ciudades> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Ciudades.class));
        return entityManager.createQuery(cq).getResultList();
    }
    
    public List<Ciudades> findByNombre(String nombre) {
        return getEntityManager().createNamedQuery("Ciudades.findByNombreCiudad")
                .setParameter("nombreCiudad", nombre + "%")
                .getResultList();
    }
}
