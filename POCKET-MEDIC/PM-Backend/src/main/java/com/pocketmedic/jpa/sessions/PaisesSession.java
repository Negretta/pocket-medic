/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Paises;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class PaisesSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Paises paises){
        entityManager.persist(paises);
    }
    
    public void update (Paises paises){
        entityManager.merge(paises);
    }
    
    public void remove (Paises citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Paises find(int id){
        return getEntityManager().find(Paises.class, id);
    }
    
    public List<Paises> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Paises.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
