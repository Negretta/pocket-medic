/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Consultas;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class ConsultasSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public void create (Consultas consultas){
        entityManager.persist(consultas);
    }
    
    public void update (Consultas consultas){
        entityManager.merge(consultas);
    }
    
    public void remove (Consultas citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Consultas find(int id){
        return getEntityManager().find(Consultas.class, id);
    }
    
    public List<Consultas> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Consultas.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
