/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.Usuarios;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class UsuariosSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    
    public void create (Usuarios usuarios){
        entityManager.persist(usuarios);
    }
    
    public void update (Usuarios usuarios){
        entityManager.merge(usuarios);
    }
    
    public void remove (Usuarios citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public Usuarios find(int id){
        return getEntityManager().find(Usuarios.class, id);
    }
    
    public List<Usuarios> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Usuarios.class));
        return entityManager.createQuery(cq).getResultList();
    }
    
    public Usuarios findByEmail(String email) {
        try {
            return (Usuarios) entityManager.createNamedQuery("Usuarios.findByEmail")
                    .setParameter("email", email)
                    .getSingleResult();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }

    public List<Usuarios> findUsuarioByIdRol(String idRol) {
        try {
            return getEntityManager().createNamedQuery("Usuarios.findByIdRol")
                    .setParameter("idRol", idRol)
                    .getResultList();
        } catch (NonUniqueResultException ex) {
            throw ex;
        } catch (NoResultException ex) {
            return null;
        }

    }
}
