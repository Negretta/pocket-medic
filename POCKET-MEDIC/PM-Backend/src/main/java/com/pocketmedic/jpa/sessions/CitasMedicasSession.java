/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pocketmedic.jpa.sessions;

import com.pocketmedic.jpa.entities.CitasMedicas;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author emerson
 */
@Stateless
public class CitasMedicasSession {

    @EJB
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }
   
    
    public void create (CitasMedicas citasmedicas){
        entityManager.persist(citasmedicas);
    }
    
    public void update (CitasMedicas citasmedicas){
        entityManager.merge(citasmedicas);
    }
    
    public void remove (CitasMedicas citasMedicas){
        entityManager.remove(citasMedicas);
    }
    
    public CitasMedicas find(int id){
        return getEntityManager().find(CitasMedicas.class, id);
    }
    
    public List<CitasMedicas> findAll() {
        CriteriaQuery cq = entityManager.getCriteriaBuilder().createQuery();
        cq.select(cq.from(CitasMedicas.class));
        return entityManager.createQuery(cq).getResultList();
    }
}
