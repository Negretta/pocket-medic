package com.pocketmedic.jpa.entities;

import com.pocketmedic.jpa.entities.Pagos;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-05-11T10:38:45")
@StaticMetamodel(TiposDePagos_1.class)
public class TiposDePagos_1_ { 

    public static volatile SingularAttribute<TiposDePagos_1, String> descripcion;
    public static volatile SingularAttribute<TiposDePagos_1, Integer> idTipoPago;
    public static volatile ListAttribute<TiposDePagos_1, Pagos> pagosList;

}